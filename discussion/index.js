// Express set up

const express = require('express');

// Mongoose is a package that allows the creation of schemas to model our data structures.
const mongoose = require('mongoose');

const app = express();
const port = 4001;

// [Section] - Mongoose Connection
// Mongoose uses the 'connect' function to connect to the cluster in our MongoDb Atlas.

/*
	It takes 2 arguments:
	1. Connection string from our MongoDB Atlas.
	2. Object that contains the middleware/standards that MongoDB uses.
*/

mongoose.connect(`mongodb+srv://yobynnam:admin123@zuittbootcamp-b197pt.ufiwlyz.mongodb.net/s35?retryWrites=true&w=majority`, {
	// {useNewUrlParse: true} - it allows us to avoid any current and/or future error while connecting to MongoDB.
	useNewUrlParser: true,
	// {useUnified Topology is 'false' by default. Set to 'true' to opt in to using the MongoDB driver's new connection management engine.}
	useUnifiedTopology: true
});

// Initializes the mongoose connection to the MongoDB database by assigning 'mongoose.connection' to the 'db' variable.
let db = mongoose.connection;

// Listen to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console base on the event (error or successful).
db.on('error', console.error.bind(console, 'Connection'));
db.once('open', () => console.log('Connected to MongoDB!'));



// CREATING a SCHEMA
const taskSchema = new mongoose.Schema({
	// Define the fields with their corresponding data types
	// For a task, it needs a "task name" and its data type will be "String".
	name: String,
	// There is a field called "status" that has a data type of "String" and the default value is "Pending".
	status: {
		type: String,
		default: 'Pending'
	}
});

// MODELS
// The variable/oject "Task" can now be used to run commands for interacting with our DB.
// "Task" is capitalized following the MVC approach for the naming convention.
// Models must be in singular form and capitalized.
// The first parameter is used to specify the Name of the collection where we will store our data.
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in our MongoDB collection/s.
const Task = mongoose.model('Task', taskSchema);

// CREATING THE ROUTES

// Middelware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Create a user route
app.post('/tasks', (req, res) => {
	// business logic
	Task.findOne({name: req.body.name}, (error, result) => {
		if (error){
			return res.send(error);
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate task found.');
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((error, savedTask) => {
				if (error) {
					return console.error(error)
				} else {
					return res.status(201).send('New Task Created!');
				};
			});
		};
	});
});

// Get all users from collection - get all users route
app.get('/tasks', (req, res) => {
	Task.find({}, (error, result) => {
		if (error) {
			return res.send(error);
		} else {
			return res.status(200).json({
				task: result
			});
		};
	});
});

// Activity

// User schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// User model
const User = mongoose.model('User', userSchema);


// Create a user route
app.post('/signup', (req, res) => {
	// business logic
	User.findOne({username: req.body.username}, (error, result) => {
		if (error){
			return res.send(error);
		} else if (result != null && result.username == req.body.username) {
			return res.send('Duplicate user found.');
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((error, savedUser) => {
				if (error) {
					return console.error(error)
				} else {
					return res.status(201).send('New User registered!');
				};
			});
		};
	});
});

app.listen(port, () => console.log(`Server is running at port: ${port}`));